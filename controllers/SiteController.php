<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    
    public function actionUno(){
        
        return $this->render('hola');
    }
    public function actionDos(){
        $texto="ejemplo de camion";
        return $this->render("dos",["texto"=>$texto]);
    }
    public function actionTres(){
        
        return $this->render("tres");
        
    }
    
    
    public function actionFormulario()
{
    $model = new \app\models\Formulario();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
             return $this->render("solucion3", [
                        "model" => $model,
                ]);

        }
    }

    return $this->render('formulario', [
        'model' => $model,
    ]);
}
   
}
